(function($) {

  $('nav#nav li.submenu ul').hide();
  $('nav#nav li.submenu a.item').on('click', function(){
    $(this).parent('li.submenu').find('ul').toggle();
    return false;
  });

  var $translations = {
    en: {
      "Patrocinadores": ["Sponsors"],
      "Diamante": ["Diamond"],
      "Ouro": ["Gold"],
      "Prata": ["Silver"],
      "Bronze": ["Bronze"],
      "Promoção": ["Promotion"],
      "Organização": ["Organization"],
      "Painel de Controle": ["Control Panel"],
      "Desenvolvidor por": ["Developed by"],
      "Licença Creative Commons": ["Creative Common License"],
      "Todo o conteúdo e dados do portal/site CBSoft.org estão disponíveis sob a licença": ["All content and portal data/site CBSoft.org are available under license"],
      "Creative Commons Atribuição 3.0 Unported (CC BY 3.0)": ["Creative Commons Attribution 3.0 Unported (CC BY 3.0)"],
      "exceto quando especificado em contrário e nos conteúdos replicados de outras fontes": ["except where specified otherwise replicated and content from other sources"]
    }
  }

  var language = $('html').attr('lang');
  if ($translations[language]) {
    $('#theme-footer h2, #theme-footer h3, #theme-footer section.developed-by span, #theme-footer section.license-footer span').each(function() {
      var element = $(this);
      var translated = $translations[language][element.html()];
      if (translated) {
        element.html(translated[0]);
      }
    });
  }
})(jQuery);
