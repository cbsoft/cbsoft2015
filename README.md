# Temas para CBSoft 2015

Contém os seguintes diretórios, com os temas para a plataforma Noosfero:

* cbsoft2015
* sbes2015
* sbcars2015
* sblp2015
* sbmf2015

Para usá-los, copie os diretórios para a raiz do diretório
themes do Noosfero (/user/share/noosfero/public/design/themes).

Arquivos criados baseados no tema do FISL 12, disponível em
https://gitlab.com/softwarelivre-org/fisl12.
Assim, estando sob sua mesma licença.

Desenvolvido pelo RaroLabs (http://rarolabs.com.br/) e CCS-USP (HTTP://ccsl.ime.usp.br).
